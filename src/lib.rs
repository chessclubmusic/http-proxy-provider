// Copyright 2015-2019 Capital One Services, LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#[macro_use]
extern crate wascc_codec as codec;

#[macro_use]
extern crate log;

extern crate actix_rt;

use actix_web::dev::Server;
use actix_web::client::Client;
use actix_web::web::Bytes;
use actix_web::{middleware, web, App, HttpRequest, HttpResponse, HttpServer, Error};
use std::collections::HashMap;
use std::error::Error as StdError;
use std::sync::Arc;
use std::sync::RwLock;

use codec::capabilities::{
    CapabilityProvider, CapabilityDescriptor, Dispatcher, NullDispatcher, OperationDirection,
    OP_GET_CAPABILITY_DESCRIPTOR,
};
use codec::{core::CapabilityConfiguration, http::OP_HANDLE_REQUEST};
use codec::core::{OP_BIND_ACTOR, OP_REMOVE_ACTOR};
use codec::{deserialize, serialize};

/// Unique identifier for the capability being provided. Note other providers can
/// provide this same capability (just not at the same time)
const CAPABILITY_ID: &str = "wascc:http_proxy";
const VERSION: &str = env!("CARGO_PKG_VERSION");
const REVISION: u32 = 1;

#[cfg(not(feature = "static_plugin"))]
capability_provider!(HttpProxyProvider, HttpProxyProvider::new);

pub struct HttpProxyProvider {
    dispatcher: Arc<RwLock<Box<dyn Dispatcher>>>,
    servers: Arc<RwLock<HashMap<String, Server>>>,
}

impl HttpProxyProvider {
    // Creates a new HTTP proxy provider. This is automatically invoked
    // by dynamically loaded plugins, and manually invoked by custom hosts
    // with a statically-linked dependency on this crate
    pub fn new() -> Self {
        Self::default()
    }

    /// Stops a running web server, freeing up its associated port
    fn terminate_server(&self, module: &str) {
        {
            let lock = self.servers.read().unwrap();
            if !lock.contains_key(module) {
                error!(
                    "Received request to stop server for non-configured actor {}. Igoring.",
                    module
                );
                return;
            }
            let server = lock.get(module).unwrap();
            let _ = server.stop(true);
        }
        {
            let mut lock = self.servers.write().unwrap();
            lock.remove(module).unwrap();
        }
    }

    /// Starts a new web server and binds to the appropriate port
    fn spawn_server(&self, cfgvals: &CapabilityConfiguration) {
        let bind_addr = match cfgvals.values.get("PORT") {
            Some(v) => format!("0.0.0.0:{}", v),
            None => "0.0.0.0:8080".to_string(),
        };
        
        let proxied_url = match cfgvals.values.get("PROXIED_URL") {
            Some(v) => format!("{}", v),
            None => "http://127.0.0.1:9000".to_string(),
        };


        let disp = self.dispatcher.clone();
        let module_id = cfgvals.module.clone();
        let duplicate_config = cfgvals.clone();

        info!("Received HTTP Proxy configuration for {}", module_id);
        info!("proxied_url: {}", proxied_url);
        let servers = self.servers.clone();

        // XXX
        // TODO: need to construct the forwarding address 
        std::thread::spawn(move || {
            let module = module_id.clone();
            //let proxy_url = proxied_url.clone();
            let config_data = duplicate_config.clone();
            // XXX
            // actix_rt System is async executor
            let sys = actix_rt::System::new(&module);
            let server = HttpServer::new(move || {
                App::new()
                    .app_data(web::PayloadConfig::new(1 << 25))
                    .wrap(middleware::Logger::default())
                    .data(disp.clone())
                    .data(config_data.clone())
                    .default_service(web::route().to(request_handler))
            })
            .bind(bind_addr)
            .unwrap()
            .disable_signals()
            .run();
            servers.write().unwrap().insert(module_id.clone(), server);

            let _ = sys.run();
        });
    }

    // obtains the capability provider descriptor
    fn get_descriptor(&self) -> Result<Vec<u8>, Box<dyn StdError + Sync + Send>> {
        Ok(serialize(
            CapabilityDescriptor::builder()
                .id(CAPABILITY_ID)
                .name("waSCC HTTP Proxy Provider (Actix)")
                .long_description("Serves as an HTTP proxy for a desired resource for waSCC actors")
                .version(VERSION)
                .revision(REVISION)
                .with_operation(
                    OP_HANDLE_REQUEST,
                    OperationDirection::ToActor,
                    "Proxies an HTTP request for an actor",
                )
                .build(),
        )?)
    }
}

impl Default for HttpProxyProvider {
    fn default() -> Self {
        match env_logger::try_init() {
            Ok(_) => {}
            Err(_) => println!("** HTTP provider: Logger already initialized, skipping."),
        };
        HttpProxyProvider {
            dispatcher: Arc::new(RwLock::new(Box::new(NullDispatcher::new()))),
            servers: Arc::new(RwLock::new(HashMap::new())),
        }
    }
}

impl CapabilityProvider for HttpProxyProvider {
    fn configure_dispatch(&self,
                          dispatcher: Box<dyn Dispatcher>,
    ) -> Result<(), Box<dyn StdError + Sync + Send>> {
        info!("Dispatcher configured.");

        let mut lock = self.dispatcher.write().unwrap();
        *lock = dispatcher;

        Ok(())
    }

    fn handle_call(
        &self,
        actor: &str,
        op: &str,
        msg: &[u8],
    ) -> Result<Vec<u8>, Box<dyn StdError + Sync + Send>> {
        info!("Handling operation `{}` from `{}`", op, actor);
        // TIP: do not allow individual modules to attempt to send configuration,
        // only accept it from the host runtime
        match op {
            OP_BIND_ACTOR if actor == "system" => {
                let cfgvals = deserialize(msg)?;
                self.spawn_server(&cfgvals);
                Ok(vec![])
            }
            OP_REMOVE_ACTOR if actor == "system" => {
                let cfgvals = deserialize::<CapabilityConfiguration>(msg)?;
                info!("Removing actor configuration for {}", cfgvals.module);
                self.terminate_server(&cfgvals.module);
                Ok(vec![])
            }
            OP_GET_CAPABILITY_DESCRIPTOR if actor == "system" => self.get_descriptor(),
            _ => Err("bad dispatch".into()),
        }
    }
}

// Passes incoming request on to proxied server, returns response to client
async fn request_handler(
    req: HttpRequest,
    payload: Bytes,
    state: web::Data<Arc<RwLock<Box<dyn Dispatcher>>>>,
    config_data: web::Data<CapabilityConfiguration>,
) -> Result<HttpResponse, Error> {
    let peer = req.head().peer_addr.unwrap().to_string().clone();
    let addr = peer.get(0..peer.find(':').unwrap()).unwrap();
    let proxied_url = match config_data.get_ref().values.get("PROXIED_URL") {
        Some(v) => format!("{}", v),
        None => "http://127.0.0.1:9000".to_string(),
    };
    let module_id = config_data.get_ref().module.clone();
    let forward_uri = match req.uri().query() {
        Some(query) => format!("{}{}?{}", proxied_url, req.uri().path(), query),
        None => format!("{}{}", proxied_url, req.uri().path()),
    };

    let client = Client::default();
    let forwarded_req = client
        .request_from(forward_uri, req.head())
        .no_decompress();

    let forwarded_req = forwarded_req.set_header(
        "X-Forwarded-For",
        addr,
    );
    debug!("FORWARDED method: {} path: {} query_string: {}",
        forwarded_req.get_method().as_str().to_string(),
        forwarded_req.get_uri().path().to_string(),
        forwarded_req.get_uri().query().unwrap_or("").to_string());
    debug!("payload.len(): {}", payload.len());

    debug!("** PROXY PROVIDER: proxied_url: {} module: {} **", proxied_url, module_id);
    debug!("XXX ***Proxy provider: constructing request");
    let request = codec::http::Request {
        method: forwarded_req.get_method().as_str().to_string(),
        path: forwarded_req.get_uri().path().to_string(),
        query_string: forwarded_req.get_uri().query().unwrap_or("").to_string(),
        header: extract_headers(&forwarded_req),
        body: payload.to_vec()
    };
    let buf = serialize(request).unwrap();

    let resp = {
        let lock = (*state).read().unwrap();
        lock.dispatch(&module_id, "HandleRequest", &buf)
    };
    match resp {
        Ok(_r) => {
            debug!("Client response is Ok!");
        }
        Err(e) => {
            error!("Guest failed to handle HTTP request: {}", e);
        }
    }
    debug!("XXX ***Proxy provider: passing request on to Minio server");

    let mut res = forwarded_req.send_body(payload).await.map_err(Error::from)?;

    let mut client_resp = HttpResponse::build(res.status());
    debug!("res.headers().len(): {}", res.headers().len());
    for (hname, hval) in res.headers().iter() {
        debug!("hname: {} hval: {}", hname, hval.to_str().unwrap());
        client_resp.set_header(hname, hval.to_str().unwrap());
    }

    //XXX don't just wrap in Ok(), match result!
    Ok(client_resp.body(res.body().await?))
}

fn extract_headers(req: &actix_web::client::ClientRequest) -> HashMap<String, String> {
    let mut hm = HashMap::new();

    for (hname, hval) in req.headers().iter() {
        hm.insert(
            hname.as_str().to_string(),
            hval.to_str().unwrap().to_string(),
        );
    }

    hm
}
