![travis](https://travis-ci.org/wascc/http-server-provider.svg?branch=master)&nbsp;

# waSCC HTTP Reverse Proxy Provider
This library is a _native capability provider_ for the `wascc:http_proxy` capability. Only actors signed with tokens containing this capability privilege will be allowed to use it. 

It should be compiled as a native linux (`.so`) binary and made available to the **waSCC** host runtime as a plugin. 

To create an actor that makes use of this capability provider, make sure that a configuration is supplied at runtime and includes a `PORT` variable. This will enable the HTTP reverse proxy and direct _all_ requests to your actor module, which you can handle by checking that a dispatched operation is equivalent to the constant `OP_HANDLE_REQUEST`.

**CAUTION**: Be careful not to request the same HTTP port for multiple actors in the same host process, as this will cause the host process to fail.
